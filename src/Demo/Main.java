package Demo;

public class Main {
    public static void main(String[] args) {
        Main m = new Main();
        m.inici();
    }
    private void inici(){
        Ficheros f=new Ficheros();
        f.llegir("paraules.txt");
        f.llegirAmbScanner("paraulesCurt.txt");
        f.escriureFitxerText("sortida.txt");
        f.escriureFitxerUTF8("paraulesUTF.txt");
    }
}
