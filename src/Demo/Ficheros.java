package Demo;

import java.io.*;
import java.util.Scanner;

public class Ficheros {
    private final String MARCA_FI ="xxxxx";

    public void llegir(String nomArxiu) {
        try {
            File f = new File(nomArxiu);
            FileReader fr = new FileReader(f);
            int dades = fr.read();
            while (dades != -1) {
                System.out.print((char) dades);
                dades = fr.read();
            }
            fr.close(); //Cal tancar el fitxer
            System.out.println("\n");
        } catch (Exception e) {
            System.out.println("Error llegint fitxer: " + e);
        }
    }
    public void llegirAmbScanner (String nomArxiu) {
        Scanner lector=null;
        try {
            File f = new File(nomArxiu);
            lector = new Scanner(f);
            boolean llegir = true;
            while (llegir) {
                String paraula = lector.next();
                if (MARCA_FI.equals(paraula)) {
                    llegir = false;
                } else {
                    System.out.println(paraula);
                }
            }
            System.out.println("\n");
        } catch(Exception e) {
            System.out.println("Error llegint fitxer: " + e);
        }finally {
            lector.close(); //Cal tancar el fitxer
        }
    }
    public void escriureFitxerText(String nomArxiu){

        File f = new File(nomArxiu);
        FileWriter fw = null;
        String cadena ="Prova de FileWriter\n";
        String[] prov = {"Albacete","Avila","Badajoz","Cáceres","Huelva","Jaén",
                "Madrid","Segovia","Soria","Toledo","Valladolid","Zamora"};
        char[] cad = cadena.toCharArray();
        try {
            //escriure els caracters d'un array un a un
            fw = new FileWriter(f);
//for (char c : cad) fw.write(c);
            for(int i=0; i<cad.length; i++)
                fw.write(cad[i]);
            //afegir al final un *
            fw.append('*');
            fw.write('\n');
            //escriure un array de caracters amb una sola instrucció
            fw.write(cad);
            // escriure un String
            fw.write("\n");
            //escriure els Strings d'un array un a un
            for(int i=0; i<prov.length; i++) {
                fw.write(prov[i]);
                fw.write("\n");
            }
            //tancar el fitxer
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void escriureFitxerUTF8(String nomArxiu){
        try {
            PrintStream out =
                    new PrintStream(nomArxiu, "UTF-8");//StandardCharsets.UTF_8
            System.setOut(out);
            System.out.println("Hello world!");
            System.out.println("\u4e16\u754c\u4f60\u597d\uff01");
            System.out.println("aáàä ÁÀÄ Ññ Çç");
        } catch (Exception e) {
        }
    }
}
