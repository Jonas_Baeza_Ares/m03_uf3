package Correus;


import java.io.*;
import java.util.Scanner;

public class DataSource {

    public String[][] getNameAndSurname(String filename){
        Correu c = new Correu();
        int pos=0;
        String [][] NomICongnom=new String[c.contarLineas()][2];
        Scanner lector=null;

        try{
            File f = new File(filename);
            lector = new Scanner(f);
            while (true){
                for (int i = 0; i < NomICongnom.length; i++) {
                    for (int j = 0; j < 1; j++) {
                        NomICongnom[pos][0]=lector.next();
                        NomICongnom[pos][1]=lector.next();
                        lector.nextLine();
                        pos++;
                    }
                }
            }
        }catch (Exception e){
            //System.out.println("Error leyendo"+e);

        }finally {
            lector.close();
        }
        return NomICongnom;
    }
    public void printaOut(String filename){
        Scanner lector=null;
        try{
            File f = new File(filename);
            lector = new Scanner(f);
            while (true){
                System.out.println(lector.next() + "\t\t\t" + lector.next());
            }
        }catch (Exception e){
            System.out.println("Se ha leido.");
        }finally {
            lector.close();
        }
    }

}
