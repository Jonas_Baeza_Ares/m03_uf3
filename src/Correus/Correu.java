package Correus;

import javax.imageio.IIOException;
import java.io.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;


public class Correu {
    DataSource ds=new DataSource();
    public int contarLineas(){
        int numeroLineas=0;
        try{
            FileReader fr = new FileReader("noms.in.dat");
            BufferedReader bf = new BufferedReader(fr);
            String linea="";
            while ((linea = bf.readLine())!=null){
                numeroLineas++;
            }
        }catch (FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return numeroLineas;
    }

    private  String fechaHex(){
        Calendar cal=new GregorianCalendar();
        String ano="";
        ano=Integer.toHexString(cal.get(Calendar.YEAR));
        return ano;
    }
    public String contrasena(){
        Random r = new Random();
        String num="";
        char numRandom=' ';
        int numero=0;
        String contr="";
        String nums="123456789";
        String letras="abcdefghijklmnopqrstuvwxyz";
        String lETRAS="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i = 0; i <= 8; i++) {
            if (i==0){
                contr=String.valueOf(lETRAS.charAt(r.nextInt(lETRAS.length())));
            }else if (i > 0 && i < 8){
                contr+=String.valueOf(letras.charAt(r.nextInt(letras.length())));
            }else{
                contr+=nums.charAt(r.nextInt(nums.length()));
            }
        }
        return contr;
    }
    public void datos(String archivo){
        File f = new File(archivo);
        FileWriter fw = null;
        try{
            fw = new FileWriter(f);
            for (int i = 0; i <contarLineas(); i++) {
                fw.write(ds.getNameAndSurname("noms.in.dat")[i][0] + "." + ds.getNameAndSurname("noms.in.dat")[i][1] + "." + fechaHex() + "@itb.kat" + "\t\t\t" + contrasena());
                if ( i < contarLineas()-1){
                    fw.write("\n");
                }
            }
            fw.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        ds.printaOut("Correus.out");
    }

}
