package Practica2FromFiles;


import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

public class DataSource {
    public String getDataFromFile(String fileName){
        String frase="";
        try {
            File f = new File(fileName);
            FileReader fr = new FileReader(f);
            int dades = fr.read();
            while (dades != -1) {
                frase=frase+(char) dades;
                dades = fr.read();
            }
            fr.close(); //Cal tancar el fitxer
            System.out.println("\n");
        } catch (Exception e) {
            System.out.println("Error llegint fitxer: " + e);
        }
        return frase;
    }
    public String getDataFromKeyboard() {
        Scanner teclat=new Scanner(System.in);
        String frase="";
        System.out.println("Introdueix la frase:");
        frase= teclat.nextLine();
        return frase;
    }


}
