/*
@use: Clase per obtenir les dades.
@author: Jonas Baeza Ares
@since: 14/04/21


package Practica3FromFiles;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DataSource {
    public String[] getWordsFromFile(String fileName){
        List<String>lParaules = new ArrayList<>();
        Scanner lector=null;
        try {
            File f = new File(fileName);
            lector = new Scanner(f);
            boolean llegir = true;
            while (llegir) {
                String paraula = lector.next();
                lParaules.add(paraula);
            }
            System.out.println("\n");
        } catch(Exception e) {
            System.out.println("Error llegint fitxer: " + e);
        }finally {
            lector.close(); //Cal tancar el fitxer
        }
        String[] paraules = lParaules.toArray(new String[0]);
        return paraules;
    }

  /*  private final String[] PARAULES ={"caben","ebrio","facha","radio","pagar","mafia","obras","aforo",
            "almas","amigo", "ancho","tazas","valsa","pizza"};


    public String[] getWords(){
        return PARAULES;
    }


    public String getDataFromKeyboard() {
        Scanner teclat=new Scanner(System.in);
        String paraula="";
        System.out.println("Introdueix la paraula:");
        paraula= teclat.nextLine();
        return paraula;
    }


}



Si volgués fer el joc sense accents utilitzaria aquest mètode però, he pensat que la ortografia es important per poder jugar correctament.
.............................................
String cadena="áüâíóù";
System.out.println(Normalizer.normalize(cadena, Normalizer.Form.NFD) .replaceAll("[^\\p{ASCII}]", ""));


        */