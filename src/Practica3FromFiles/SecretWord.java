/*
@use: Clase per processar les dades.
@author: Jonas Baeza Ares
@since: 14/04/21


package Practica3FromFiles;
import java.util.Random;
import java.util.Scanner;

public class SecretWord {

    DataSource ds = new DataSource();
    String keyWord="";
    String paraula="";
    int clletraYposicio = 0;
    int contador=10;


    public void menu(){
        String opcioMenu="";
        Scanner teclat= new Scanner(System.in);
        System.out.println("Que vols fer?");
        System.out.println("(J)Jugar o (S)Sortir");
        if (teclat.hasNextLine()){
            opcioMenu=teclat.nextLine();
            if (opcioMenu.equalsIgnoreCase("J")) {
                //programa
                keyWord=escollirParaula();
                paraula=ds.getWordsFromFile();
                compararParaules(keyWord, paraula);
            }else if (opcioMenu.equalsIgnoreCase("S")){
                System.out.println("Have a nice day...");
            }else{
                System.out.println("No has introduït una lletra correcte, si us plau introdueix J o S.");
                menu();
            }
        }else {
            System.out.println("No has introduït res.");
            menu();
        }
    }
    private String escollirParaula(){
        Random r=new Random();
        int valorAleatorio= r.nextInt((ds.getWordsFromFile().length));
        String keyWord=ds.getWordsFromFile()[valorAleatorio];
        System.out.println("La paraula esta escollida ara tens 10 intents per esbrinar-la:");
        return keyWord;
    }
    private void compararParaules(String keyWord, String paraula) {
        clletraYposicio = 0;
        int clletraNposicio = 0;
        for (int i = 0; i < keyWord.length(); i++) {
            for (int j = 0; j < paraula.length(); j++) {
                String keyWordCH=Character.toString(keyWord.charAt(i));
                String paraulaCH=Character.toString(paraula.charAt(j));
                if (keyWordCH.equalsIgnoreCase(paraulaCH) && i == j) {
                    clletraYposicio = clletraYposicio + 1;
                } else if (keyWordCH.equalsIgnoreCase(paraulaCH) && i != j) {
                    clletraNposicio = clletraNposicio + 1;
                }
            }
        }
        if (clletraYposicio!=5){
            System.out.println("Hi han " + clletraYposicio + " lletres iguals en la mateixa posició i " + clletraNposicio + " lletres que són iguals pero no coincideixen en la posició.");
            controlador();
        }else {
            controlador();
        }
    }
    private void controlador(){
        contador=contador-1;
        if (contador == 0){
            System.out.println("Ho sento s'han acabat els intents");
        }else if (clletraYposicio == 5){
            System.out.println("Felicitats ets un crack!!");
        }else{
            System.out.println("Et quedan "+contador+" número d'intents.");
            paraula=ds.getWordsFromFile();
            compararParaules(keyWord, paraula);
        }
    }


}

*/